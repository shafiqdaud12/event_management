import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart'as http;
import 'package:eventmanagement/eventList/eventlist.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constant.dart';

class BookingDetail extends StatefulWidget {
  const BookingDetail({Key? key}) : super(key: key);

  @override
  _BookingDetailState createState() => _BookingDetailState();
}

class _BookingDetailState extends State<BookingDetail> {

  List booking = [];
  String eventName = '';
  String bookingID = '';
  List detail=[];
  List confirm=[];

  getBooking() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token');

    var UserID = prefs.getString("userID");
    var url = "http://10.0.2.2:4000/user/booking/$UserID";

    final http.Response response = await http.get(
        url, headers: <String, String>{
      'Content-Type': "application/json; charset=UTF-8",
      HttpHeaders.authorizationHeader: 'Bearer $token'
    });
    if (response.statusCode == 200) {
      setState(() {
        detail = jsonDecode(response.body);
      });
    }
    for(int i=0;i<detail.length;i++){
      if(detail[i]['PaymentType']=='Cash')
      {
        setState(() {
          booking.add(detail[i]);
        });
      }else{
        setState(() {
          confirm.add(detail[i]);
        });
      }

    }
    print(confirm);
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getBooking();
  }

  @override
  Widget build(BuildContext context) {
    final tabController= new DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: kPrimaryColor,
            leading: IconButton(onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return EventList();
              }));
            }, icon: Icon(Icons.arrow_back_sharp),),
            title: Text('Booking screen'),
            bottom: TabBar(
              tabs: [
                Tab(text: 'Booked',),
                Tab(text: 'Purchased',)
              ],
            ),

          ),
          body: TabBarView(children: [
            Container(
              child: ListView.builder(
                itemCount: booking.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: Card(
                      child: ListTile(
                        leading: Text(booking[index]['Event_Name']),
                        title: Text(booking[index]['CategoryName']),
                        subtitle: Column(
                          children: [
                            Text(booking[index]['NumberOFTickets'].toString()),
                            Text(booking[index]['Amount'].toString()),
                            Text(booking[index]['PaymentType'].toString())
                          ],
                        ),
                        trailing: IconButton(icon: Icon(CupertinoIcons.delete), onPressed: () async {
                          var bookingID=booking[index]['bookingID'];
                          print(bookingID);

                          var url = "http://10.0.2.2:4000/user/booking/$bookingID";
                          var response= await http.delete(
                              url,headers:<String,String>{'Content-Type': 'application/json; charset=utf-8'});
                          if(response.statusCode==200){
                            Navigator.push(context, MaterialPageRoute(builder: (context){
                              return BookingDetail();
                            }
                            ));
                          }

                        },),
                      ),
                    ),
                  );
                  
                },
              ),
            ),

            //PurchasedTIckets container
            Container(
              child: ListView.builder(
                itemCount: confirm.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: Card(
                      child: ListTile(
                        leading: Text(confirm[index]['Event_Name']),
                        title: Text(confirm[index]['CategoryName']),
                        subtitle: Column(
                          children: [
                            Text(confirm[index]['NumberOFTickets'].toString()),
                            Text(confirm[index]['Amount'].toString()),
                            Text(confirm[index]['PaymentType'].toString())
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],),

        )
    );
    return MaterialApp(
      home: tabController,
    );
  }

}
