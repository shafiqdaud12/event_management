import 'package:eventmanagement/constant.dart';
import 'package:eventmanagement/login/loginScreen.dart';
import 'package:flutter/material.dart';
import 'package:eventmanagement/welcome.dart';
import 'package:get/get.dart';

void main()=>{
  runApp(MyApp())
};

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Event Management',
      theme: ThemeData (
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: Colors.white
      ),
      home: WelcomeScreen(),
    );

  }
}
