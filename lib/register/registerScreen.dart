import 'dart:convert';

import 'package:eventmanagement/constant.dart';
import 'package:eventmanagement/login/loginScreen.dart';
import 'package:eventmanagement/login/password_fieldRounded.dart';
import 'package:eventmanagement/login/roundedButton.dart';
import 'package:eventmanagement/login/rounded_inputfield.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


class registerScreen extends StatelessWidget {
  const registerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
    );
  }
}

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
    String EmailAddress='';
    String FName='';
    String LName='';
    String password='';
    String confirmPassword='';
    String phoneNumber='';

    return SingleChildScrollView(
      child: Container(
        height: size.height,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('SIGN UP', style: TextStyle(fontWeight: FontWeight.bold,color: kPrimaryColor,fontSize: 32),),
            RoundedFieldContainer(hintText: 'Enter Your Email', onChanged: (value){
              EmailAddress=value;
            }),
            roundedPasswordField(onChanged: (value){
              password=value;
            },hintText: 'Enter Password', ),
            roundedPasswordField(onChanged: (value){
              confirmPassword=value;
            },hintText:  'Confirm Password ', ),
            RoundedFieldContainer(hintText: 'First Name', onChanged: (value){
              FName=value;
            }),
            RoundedFieldContainer(hintText: 'Last Name', onChanged: (value){
              LName=value;
            }),
            RoundedFieldContainer(hintText: 'Enter Your Phone Number', onChanged: (value){
              phoneNumber=value ;
            }),
            RoundedButton(text: "SIGN UP", press: () async {
              if(password==confirmPassword){
                var url='http://10.0.2.2:4000/user';
                final http.Response response=await http.post(
                  url,headers: <String, String>{
                  'Content-Type': 'application/json ; charset=UTF-8',
                },
                  body: jsonEncode(<String, String>{
                    'EmailAddress':EmailAddress, 'FName':FName,'LName':LName,'password':password,'phoneNumber':phoneNumber
                  }),
                );

                if(response.statusCode==403){
                  AlertDialog alert= AlertDialog(title: Text("Email already exist"),
                    content: Text("User already exists. Enter new email."),
                    actions: [],);
                  showDialog(context: context, builder: (BuildContext context){
                    return alert;
                  });
                }else if(response.statusCode==200 && password == confirmPassword) {
                  print(password);
                  print(confirmPassword);
                  if( password.length >=8 ){
                    AlertDialog alert=AlertDialog(title: Text("Registration Successful"),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                    );
                    showDialog(context: context, builder: (BuildContext context){
                      return alert;
                    });
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return loginScreen();
                    }));

                  }
              }
              else{
                  AlertDialog alert=AlertDialog(title: Text("Password does not match"),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                    );

                  showDialog(context: context, builder: (BuildContext context){
                    return alert;
                  });
                }


              }


            })

          ],

        ),
      ),
    );

  }


}

