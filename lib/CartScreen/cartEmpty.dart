
import 'package:flutter/material.dart';

class cartEmpty extends StatefulWidget {
  const cartEmpty({Key? key}) : super(key: key);

  @override
  _cartEmptyState createState() => _cartEmptyState();
}

class _cartEmptyState extends State<cartEmpty> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Text('Your cart is empty',style: TextStyle(fontSize: 32,fontWeight: FontWeight.bold),)),

    );
  }
}
