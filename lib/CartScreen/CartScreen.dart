import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:eventmanagement/CartScreen/cartEmpty.dart';
import 'package:eventmanagement/cardScreen/cardScreen.dart';
import 'package:eventmanagement/eventList/eventlist.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart'as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../constant.dart';

class BookingScreen extends StatefulWidget {
  const BookingScreen({Key? key}) : super(key: key);

  @override
  _BookingScreenState createState() => _BookingScreenState();
}

class _BookingScreenState extends State<BookingScreen> {

  List cart=[];
  int n=1;
  int amount=0;
  bool checkCart=false;

  getCartDetail() async {

    setState(() {
      checkCart=true;
    });
    SharedPreferences prefs=await SharedPreferences.getInstance();
    var UserID=prefs.getString("userID");
    var url = "http://10.0.2.2:4000/user/cart/$UserID";

    final http.Response response=await http.get(url,headers: <String,String>{'Content-Type': 'application/json; charset=utf-8'});
    if(response.statusCode==200){
      setState(() {
        checkCart=false;
        cart=jsonDecode(response.body);
      });
      if(cart.isEmpty){
        setState(() {
          checkCart=true;
        });
      }

      for(int i=0;i<cart.length;i++){

        setState(() {
          amount= cart[i]['Amount'] + amount;
        });
        prefs.setString('amount', amount.toString());

      }

    }

  }


  void initState() {
    super.initState();
    getCartDetail();
  }

  @override
  Widget build(BuildContext context) {

    Size size=MediaQuery.of(context).size;
    //debugger();
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(icon: Icon(Icons.arrow_back_sharp), onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context){
              return EventList();
            }));
            },),
        title: Text('Cart'),
        backgroundColor: kPrimaryColor,
      ),

      body: checkCart ? cartEmpty():
      SingleChildScrollView(
        child: Column(
          children:[
            Padding(
              padding: EdgeInsets.symmetric(vertical:25 ),
              child: Column(
                children:[
                  Container(
                    height: size.height*.75,
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: cart.length,
                        itemBuilder: (context,index){
                          // debugger();
                          return Container(
                            child: Card(
                              elevation:5,
                              shadowColor: kPrimaryColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Row(
                                children:[

                                  CartProducts(
                                    Event_Name: cart[index]['Event_Name'], Price: cart[index]['Price'].toString(),
                                    CategoryName: cart[index]['CategoryName'],
                                    AvailTickets: cart[index]['AvailTickets'].toString(),
                                    amount: cart[index]['Amount'].toString(),
                                    purchased: cart[index]['TicketPurchased'].toString(),
                                  ),
                                  Spacer(),
                                  IconButton(
                                      onPressed: () async {
                                        var CartID= cart[index]['CartID'].toString();
                                        //debugger();
                                        var url1 = "http://10.0.2.2:4000/user/cart/$CartID";
                                        var response= await http.delete(
                                            url1,headers:<String,String>{'Content-Type': 'application/json; charset=utf-8'});
                                        if(response.statusCode==200){
                                          return showDialog(context: context, builder: (BuildContext context){
                                            return new AlertDialog(
                                                title: Text('Product Deleted'),
                                                content: ElevatedButton(onPressed: () {
                                                  Navigator.push(context, MaterialPageRoute(builder: (context){
                                                    return BookingScreen();
                                                  }));
                                                },
                                                  child: Text('Ok'),)
                                            );
                                          });
                                        }else if(response.statusCode==404){
                                          Get.snackbar('Product Failed to delete','');
                                        }
                                      }, icon: Icon(Icons.close)),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                  SizedBox(height: 22,),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: 4),
                      borderRadius: BorderRadius.circular(10)


                    ),
                    child: Row(
                      children:[
                        Text('Total amount: ',style: TextStyle(fontWeight: FontWeight.bold), ),
                        SizedBox(width:5),
                        Text( amount.toString(),style: TextStyle(fontSize: 30),),
                        Spacer(),
                        ElevatedButton(onPressed: (){
                           showDialog(context: context,builder: (BuildContext context){
                            return AlertDialog(
                              scrollable: true,
                              title: Column(children:[
                                Text('Kindly select the payment type'),
                                SizedBox(height: 10,),
                                Text('Amount to be paid is: $amount '),
                                SizedBox(height: 10,),
                                Text('Select Cash for booking of ticket. Please arrive half an hour early to purchase tickets'),
                                SizedBox(height: 10,),
                                Text('Select Visa/MasterCard for purchasing ticket.'),



                              ] ),
                              content: Align(
                                alignment: Alignment.center,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding:EdgeInsets.only(left: 45) ,
                                      child: ElevatedButton(onPressed: () async {

                                        SharedPreferences prefs= await SharedPreferences.getInstance() ;
                                        var userID=prefs.getString('userID');
                                        var token=prefs.getString('token');
                                        var url="http://10.0.2.2:4000/user/booking";
                                        for(int i =0; i<cart.length;i++){
                                          print(cart[i]['Amount']);
                                          final http.Response response=await http.post(
                                              url,headers:<String, String>{
                                              'Content-type': 'application/json; charset=utf-8',HttpHeaders.authorizationHeader: 'Bearer $token'
                                              },
                                              body:jsonEncode(<String, String>{
                                              'UserID': userID,'TicketID':cart[i]['TicketID'].toString(), 'CategoryName': cart[i]['CategoryName'],
                                              'NumberOFTickets': cart[i]['TicketPurchased'].toString(),
                                              'PaymentType':'Cash', 'Amount':cart[i]['Amount'].toString(), 'Event_Name': cart[i]['Event_Name']
                                              }));
                                          if(response.statusCode==200){
                                            Navigator.push(context, MaterialPageRoute(builder: (context){
                                              return EventList();
                                            }));
                                            Get.snackbar('Booking Successful', '');

                                            var userID=cart[i]['UserID'];
                                            print(userID);
                                            var url="http://10.0.2.2:4000/user/cart/$userID";
                                            var response=await http.delete(
                                              url,headers: <String, String>{
                                              'Content-Type': 'application/json; charset=UTF-8',
                                            },);

                                            if(response.statusCode==200){
                                              print('hello');
                                            }else{print('bye');}


                                          }
                                          if(response.statusCode==403){
                                            Get.snackbar("Booking failed try again later","");



                                          }


                                        }
                                      }, child: Text('By Cash'),),
                                    ),
                                    SizedBox(width: 10,),
                                    ElevatedButton(onPressed: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context){
                                         return CreditCardPage();
                                      }));
                                    }, child: Text('By Card'),),
                                  ],
                                ),
                              ),
                            );
                          });
                        }, child: Text('CheckOut')),
                      ]
                    ),
                  )

                ]
              ),
            ),

          ],
        ),
      ),

    );

  }
}



class CartProducts extends StatefulWidget {
  String Event_Name='';
  String Price='';
  String CategoryName='';
  String AvailTickets='';
  String amount='';
  String purchased='';

  CartProducts({required this.amount,required this.Event_Name,required this.Price,required this.CategoryName,required this.AvailTickets,required this.purchased});

  @override
  _CartProductsState createState() => _CartProductsState();
}

class _CartProductsState extends State<CartProducts> {
  int n=1;

  amount(int price,int quantity){
    return price* quantity;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 15, 0, 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                 children: [
                   Text('Name:',style: TextStyle(fontWeight: FontWeight.bold)),
                   SizedBox(width:24),
                   Text(widget.Event_Name,style: TextStyle(fontSize: 18,)),
                ],
              ),
            ),
            SizedBox(height:10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: [
                  Text('Category',style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(width:10),
                  Text(widget.CategoryName,style: TextStyle(fontSize: 18)),
                ],
              ),
            ),
            SizedBox(height:10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10,),
              child: Row(
                children: [
                  Text('Price:',style: TextStyle(fontWeight: FontWeight.bold),),
                  SizedBox(width:30),
                  Text(widget.Price,style: TextStyle(fontSize: 18)),
                ],
              ),
            ),
            //SizedBox(height:10),


            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 10),
            //   child: Row(
            //     children: [
            //       Text('Available:',style: TextStyle(fontWeight: FontWeight.bold)),
            //       SizedBox(width:5),
            //       Text(widget.AvailTickets,style: TextStyle(fontSize: 18)),
            //     ],
            //   ),
            // ),
            SizedBox(height:10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(children:[
                Text('Quantity : ', style: TextStyle(fontWeight: FontWeight.bold), ),
                SizedBox(width:10),
                Text(widget.purchased)
              ] ),

            ),
            SizedBox(height:10),

            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(children:[
                Text('Amount: ', style: TextStyle(fontWeight: FontWeight.bold), ),
                SizedBox(width:10),
                Text(widget.amount)
              ] ),

            ),




          ],


        ),
      ),

    );

  }
}








// import 'dart:convert';
// import 'dart:developer';
// import 'dart:io';
// import 'package:flutter/rendering.dart';
// import 'package:http/http.dart' as http;
// import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// import '../constant.dart';
// class CartScreen extends StatefulWidget {
//
//
//   const CartScreen({Key? key}) : super(key: key);
//
//   @override
//   _BookingScreenState createState() => _BookingScreenState();
//
//
// }
//
//
//
// class _BookingScreenState extends State<CartScreen> {
//
//
//   var price='';
//   var availTicket='';
//   var categoryName='';
//   var eventName='';
//   int _n=0;
//   int result=0;
//   String ticketID='';
//   String dateTime='';
//  // String _val='';
//   String type="Cash";
//
//   getTicket() async {
//     SharedPreferences prefs= await SharedPreferences.getInstance();
//     setState(() {
//       price=prefs.get('price');
//       availTicket=prefs.get('ticketsAvailable');
//       categoryName=prefs.get('ticketCategory');
//       eventName=prefs.get('EventName');
//       ticketID=prefs.get('TicketID');
//       dateTime=prefs.get('dateTime');
//     });
//
//   }
//
//
//   getPrice(int price, int ticket){
//     result = price*ticket;
//     return result;
//   }
//
//   addNumber(){
//     if(_n <= int.parse(availTicket)){
//       setState(() {
//         _n++;
//       });
//
//     }
//   }
//
//   subNumber(){
//     if(_n!=0 ){
//       setState(() {
//         _n--;
//       });
//     }
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     getTicket();
//   }
//   @override
//   Widget build(BuildContext context) {
//     Size size=MediaQuery.of(context).size;
//
//     return Scaffold(
//       appBar: AppBar(
//
//         backgroundColor: kPrimaryColor,
//         title: Text("Booking Screen")
//       ),
//       body: Container(
//         decoration: BoxDecoration(
//           gradient: LinearGradient(
//             colors: [
//               Color(0XFFE1BEE7),
//               Color(0XFFAB47BC),
//             ],
//             begin: Alignment.topLeft,
//             end: Alignment.bottomRight
//           ),
//         ),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             SizedBox(height:20),
//             Text('Event Name : '  +eventName.toString(),style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),),
//             SizedBox(height:10),
//             Text('Ticket Category: '+ categoryName,style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
//             SizedBox(height:20),
//             Text('Number of Tickets Available: ' + availTicket,style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300)),
//             SizedBox(height:20),
//             Text('Price of one ticket: ' + price,style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300)),
//             SizedBox(height:20),
//             Text("DateTime: "+  dateTime.toString()),
//             Row(
//               children: <Widget>[
//                 Text('Number of Tickets to buy: ',style:TextStyle(fontSize: 18,fontWeight: FontWeight.w500)),
//                 IconButton(onPressed: (){
//                   subNumber();
//                 }, icon: Icon(Icons.remove)),
//                 Text(_n.toString()),
//                 IconButton(onPressed: (){
//                   if(_n < int.parse(availTicket)){
//                     addNumber();
//                   }
//                 }, icon: Icon(Icons.add)),
//               ],
//             ),
//             SizedBox(height:20),
//
//             Divider( thickness:3),
//             Row(
//               children: <Widget>[
//                 Text("Payment Method",style:TextStyle(fontSize:20)),
//                   Spacer(),
//                   IconButton(
//                     onPressed: (){
//                       showDialog(context: context, builder: (BuildContext context){
//                         return AlertDialog(
//                           shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.all(Radius.circular(15)),
//                           ),
//                           title: Text("Payment Method" ),
//                           content: Container(
//                             height: size.height*.30,
//                             child: Column(
//                               children: [
//                                 TextButton(onPressed: (){
//                                   setState(() {
//                                     type = "Online";
//                                     Navigator.pop(context);
//                                   });
//                                 }, child: Text("Pay via Master/Visa Card")),
//                                 TextButton(onPressed: (){
//                                   setState(() {
//                                     type = "Cash";
//                                     Navigator.pop(context);
//                                   });
//                                 }, child: Text("Pay By Cash")),
//                               ],
//                             ),
//                           ),
//                         );
//                       });
//                       },
//                     icon: Icon(Icons.edit),),
//
//               ],
//             ),
//             Padding(
//                 padding: EdgeInsets.symmetric(horizontal:10),
//                 child: Row(
//                     children: [
//                       Text("- "+ type,style:TextStyle(fontSize:18)),
//                       Spacer(),
//                       Text( getPrice(int.parse(price) , _n).toString(),style:TextStyle(fontSize:18)),
//
//
//                     ],
//                 ),
//
//             ),
//
//             Divider(thickness: 2),
//             Row(
//               children:<Widget>[
//                 ElevatedButton(onPressed: () { Navigator.pop(context); }, child: Text('Cancel'),style: ElevatedButton.styleFrom(primary: Color(0XFFAB47BC)),),
//                 SizedBox(width:15),
//                 ElevatedButton(onPressed: () async {
//                   SharedPreferences prefs= await SharedPreferences.getInstance() ;
//                   var userID=prefs.getString('userID');
//                   var token=prefs.getString('token');
//                   var url="http://10.0.2.2:4000/user/booking";
//                   final http.Response response=await http.post(
//                     url,headers:<String, String>{
//                       'Content-type': 'application/json; charset=utf-8',HttpHeaders.authorizationHeader: 'Bearer $token'
//                   },
//                      body:jsonEncode(<String, String>{
//                        'UserID': userID,'TicketID':ticketID, 'CategoryName': categoryName,'NumberOFTickets':_n.toString(),
//                        'PaymentType':type, 'Amount':result.toString(), 'Event_Name': eventName
//                      })
//
//                   );
//                   if(response.statusCode==200){
//                     setState(() {
//                       availTicket=(int.parse(availTicket) - _n).toString();
//
//                     });
//                     //debugger();
//                     SharedPreferences prefs=await SharedPreferences.getInstance();
//                     prefs.setString("ticketsAvailable", availTicket);
//                     print( availTicket);
//
//                     return showDialog(context: context, builder: (BuildContext Context){
//                       return AlertDialog(
//                         shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
//                         title: Text('Ticket booked Successfully'),
//                         content: Container(
//                           height:size.height*.15,
//                           child: Column(
//                             children:[
//                               Text('Thank you for selecting us. Enjoy your event'),
//                               ElevatedButton(
//                                 onPressed: ()  async {
//                                  // debugger();
//
//                                   var url1="http://10.0.2.2:4000/user/ticket/$ticketID";
//                                   final http.Response response1=await http.put(
//                                       url1,headers:<String, String>{
//                                     'Content-type': 'application/json; charset=utf-8' },
//                                       body:jsonEncode(<String, String>{
//                                         'TicketsAvailable':availTicket
//                                       })

//
//                                   );
//                                   if(response.statusCode==200){
//                                     Navigator.of(context, rootNavigator: true).pop();
//                                     Navigator.pop(context);
//                                     var count=0;
//                                     Navigator.popUntil(context,(route){
//                                       return count ++ ==1;
//                                     });
//                                   }
//
//
//                                   },
//                                 child: Text('Ok'),),
//                             ],
//                           ),
//                         ),
//                       );
//
//                     });
//
//                   }
//                 },
//
//                     child: Text('Book Now'),style: ElevatedButton.styleFrom(primary: Color(0XFFAB47BC))
//                 ),
//
//               ],
//             ),
//           ],
//         ),
//       ),
//
//     );
//   }
// }
//
