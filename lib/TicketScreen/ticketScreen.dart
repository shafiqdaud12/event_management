import 'dart:convert';
import 'dart:core';
import 'dart:developer';
import 'dart:io';
import 'dart:ui';
import 'package:eventmanagement/CartScreen/CartScreen.dart';
import 'package:eventmanagement/constant.dart';
import 'package:eventmanagement/listEmpty/ListEmpty.dart';
import 'package:eventmanagement/loading/loadingScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart'as http;
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';

import '../constant.dart';
class ticketScreen extends StatefulWidget {

  String Event_ID;

  ticketScreen({required this.Event_ID});
  @override
  _ticketScreenState createState() => _ticketScreenState(Event_ID);

}

class _ticketScreenState extends State<ticketScreen> {

  List eventDetail=[];
  String Event_ID;
  String userID='';
  List ticketCategories=[];
  List bookingInfo=[];
  int result=0;
  int price=0;
  String TicketID='';
  String ticketBuy='';
  _ticketScreenState(this.Event_ID);
  String availTicket='';
  List category=[];
  String dateTime='';

  int _n=0;



  getTicketCategory() async{
    var url="http://10.0.2.2:4000/user/ticketcategory";
    SharedPreferences prefs=await SharedPreferences.getInstance();
    var token=prefs.getString('token');

    final http.Response response=await http.get(url,headers: <String,String>{
      'Content-Type':"application/json; charset=UTF-8",HttpHeaders.authorizationHeader:'Bearer $token '});

    if(response.statusCode==200) {
      setState(() {
        ticketCategories = json.decode(response.body);
      });

      return ticketCategories;

      //print(detail);

    }
  }
  bool loading=false;
  bool listempty=false;

   getTicketDetail(EventID) async{

     setState(() {
       loading=true;
       listempty=true;

     });

     var url="http://10.0.2.2:4000/user/ticket/$EventID";

     SharedPreferences prefs=await SharedPreferences.getInstance();
     var token=prefs.getString('token');

     final http.Response response=await http.get(url,headers: <String,String>{
       'Content-Type':"application/json; charset=UTF-8",HttpHeaders.authorizationHeader:'Bearer $token '});
     if(response.statusCode==200){
       setState(() {
         loading=false;
         listempty=false;
         eventDetail=jsonDecode(response.body);

       });
       getTicketCategory();
       if(eventDetail.isEmpty){
         listempty=true;
       }
       print(eventDetail);
     }
   }


  @override
  void initState() {
    super.initState();
    getTicketDetail(Event_ID);

  }

  TextEditingController ticketController=TextEditingController();
  @override

  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;


    return loading ? loadingScreen():
      Scaffold(
       // backgroundColor: kPrimaryColor,
        body: listempty ? ListEmpty():
        Container(
          height: size.height*.8,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 15,),
              Text(eventDetail[0]['Event_Name'],style: TextStyle(fontWeight: FontWeight.bold,fontSize:26,color: Colors.black)),
              SizedBox(height:30),
              Text(eventDetail[0]['Description'],style: TextStyle(fontWeight: FontWeight.w900,fontSize:18,color: Colors.black)),
              SizedBox(height:20),
              Text('Date  : '+ DateTime.parse(eventDetail[0]['DateTime']).toString().split(' ')[0],
                  style: TextStyle(fontSize:18,color: Colors.black,fontWeight: FontWeight.w900)),
              Text('Time : '+ DateTime.parse(eventDetail[0]['DateTime']).toString().split(' ')[1].split('.')[0],
                  style: TextStyle(fontSize:18,color: Colors.black,fontWeight: FontWeight.w900)),
              SizedBox(height:30),
              Divider(thickness:2),
              Text('Ticket Categories',style:TextStyle(color: Colors.black,fontWeight: FontWeight.w900,fontSize: 18)),
              Divider(thickness:2),
              Text('Tap on the cart icon  to add to cart',style:TextStyle(color: Colors.black,fontWeight: FontWeight.w900,fontSize: 14)),
              SizedBox(height:30),
              Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount:eventDetail.length,
                    itemBuilder: (context,index){
                      return Container(
                        child: Card(
                          color: Colors.grey[300],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: CategoriesCard(
                            availTickets: eventDetail[index]['TicketsAvailable'].toString(),
                            CategoryName: eventDetail[index]['CategoryName'],
                            price: eventDetail[index]['price'].toString(),
                            EventName: eventDetail[index]['Event_Name'],
                            DateTime: eventDetail[index]['DateTime'].toString(),
                            TicketID: eventDetail[index]['TicketID'].toString(),

                          ),
                        ),
                      );
                    }
                ),
              ),
            ],
          ),
        ),
        appBar: AppBar(
          title: Text("Book Your ticket"),
          backgroundColor: kPrimaryColor,
          actions: [
            IconButton(onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return BookingScreen();
              }));
            }, icon: Icon(Icons.shopping_cart)),
          ],
        ),

    );
  }
}


class CategoriesCard extends StatefulWidget {

  String CategoryName='';
  String price='';
  String availTickets='';
  String DateTime='';
  String EventName='';
  String TicketID='';


  CategoriesCard({required this.CategoryName,required this.price,required this.availTickets,required this.DateTime,required this.EventName,required this.TicketID});

  @override
  _CategoriesCardState createState() => _CategoriesCardState();
}

class _CategoriesCardState extends State<CategoriesCard> {
  int n=1;
  int result=0;
  amount(int price,int quantity){
    return result= price*quantity;
  }

  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
    return Container(
      width:size.width*.7,
      child: Row(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height:20),
              Row(
                children: <Widget>[
                  SizedBox(width: 10,),
                  Text('Category: ',style: TextStyle(fontWeight: FontWeight.bold),),
                  Text(widget.CategoryName,style: TextStyle(fontSize: 18)),
                ],
              ),
              SizedBox(height:10),
              Row(
                children: <Widget>[
                  SizedBox(width: 10,),
                  Text('Ticket Price: ',style: TextStyle(fontWeight: FontWeight.bold)),
                  Text(widget.price,style: TextStyle(fontSize: 18)),
                ],
              ),
              SizedBox(height:10),
              // Row(
              //   children: <Widget>[
              //     SizedBox(width: 10,),
              //     Text('Number of Tickets Available: ',style: TextStyle(fontWeight: FontWeight.bold)),
              //     Text(widget.availTickets,style: TextStyle(fontSize: 18)),
              //   ],
              // ),

              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [
                    Text('Quantity: ',style: TextStyle(fontWeight: FontWeight.bold)),
                    //SizedBox(width:10),
                    IconButton(onPressed: () async {
                      if(n>1){
                        setState(() {
                          n--;
                        });
                      }
                    }, icon: Icon(Icons.remove)),
                    Text(n.toString()),
                    IconButton(onPressed: (){
                      if(n < int.parse(widget.availTickets) )
                        setState(()  {
                          n++;
                        });
                    },icon: Icon(Icons.add),),

                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(12, 0, 0, 0),
                child: Row(
                  children: [
                    Text('Amount:  ',style: TextStyle(fontWeight: FontWeight.bold),),
                    Text(amount(int.parse(widget.price), n).toString())
                  ],
                ),
              ),

              Row(
                  children: <Widget>[
                    SizedBox(width: size.width*.3 ,),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: kPrimaryColor),

                      onPressed: () async {
                        var url="http://10.0.2.2:4000/user/cart";
                        SharedPreferences prefs=await SharedPreferences.getInstance();
                        var UserID=prefs.getString("userID");
                        final http.Response response=await http.post(url,headers:<String,String>{
                          'Content-Type':'application/json; charset=utf-8'
                        },
                            body:jsonEncode(<String,String>{
                              'Event_Name':widget.EventName,
                              'Price':widget.price ,
                              'CategoryName':widget.CategoryName,
                              'TicketPurchased':n.toString(),
                              'UserID':UserID,
                              'Amount':result.toString(),
                              'TicketID': widget.TicketID.toString()  ,
                            })
                        );
                        if(response.statusCode==200){
                          print(widget.TicketID);
                          return Get.snackbar(
                            'Item Added to cart', '',
                            snackPosition: SnackPosition.BOTTOM,
                            backgroundColor: Colors.green,
                          );
                        }else if(response.statusCode==404){
                          return Get.snackbar(
                            'Product already in cart','',
                            snackPosition: SnackPosition.BOTTOM,
                            backgroundColor: Colors.red,
                          );
                        }
                        },
                      child: Row(
                        children: [
                          Text('Add to cart'),
                          SizedBox(width:5) ,
                          Icon(Icons.add_shopping_cart,size: 26,)
                        ],
                      ),
                    ),

              ])
            ],
          ),
        ],
      ),
    );
  }
}
