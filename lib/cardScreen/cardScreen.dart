import 'dart:convert';
import 'dart:io';

import 'package:eventmanagement/CartScreen/CartScreen.dart';
import 'package:eventmanagement/eventList/eventlist.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_form.dart';
import 'package:flutter_credit_card/credit_card_model.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class CreditCardPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CreditCardPageState();
  }
}

class CreditCardPageState extends State<CreditCardPage> {
  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var amount;
  List cart=[];

  getAmount() async {
    SharedPreferences prefs=await SharedPreferences.getInstance();
    amount=prefs.getString('amount');
    print(amount);
  }

  getCartDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var UserID = prefs.getString('userID');
    var token=prefs.getString('token');

    var url = "http://10.0.2.2:4000/user/cart/$UserID";
    final http.Response response=await http.get(url,headers: <String,String>{'Content-Type': 'application/json; charset=utf-8'});
    if(response.statusCode==200){
      cart=jsonDecode(response.body);
    }
    print(cart);

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAmount();
    getCartDetail();
  }


  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: Colors.teal[50],
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.arrow_back_sharp), onPressed: () {
           Navigator.push(context, MaterialPageRoute(builder: (context){
            return BookingScreen();
          }));
        },),
        title: Text('Visa/Master Payment'),
      ),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            CreditCardWidget(
              cardBgColor: Colors.redAccent[200],
              cardNumber: cardNumber,
              expiryDate: expiryDate,
              cardHolderName: cardHolderName,
              cvvCode: cvvCode,
              showBackView: isCvvFocused,
              obscureCardNumber: true,
              obscureCardCvv: true,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    CreditCardForm(
                      formKey: formKey,
                      onCreditCardModelChange: onCreditCardModelChange,
                      obscureCvv: true,
                      obscureNumber: true,
                      cardNumberDecoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Number',
                        hintText: 'XXXX XXXX XXXX XXXX',
                      ),
                      expiryDateDecoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Expired Date',
                        hintText: 'XX/XX',

                      ),
                      cvvCodeDecoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'CVV',
                        hintText: 'XXX',
                      ),
                      cardHolderDecoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Card Holder Name',
                      ),
                    ),
                    SizedBox(height: 20,),
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Container(
                        margin: EdgeInsets.all(8),
                        child:  Text(
                          'Pay  $amount ',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      color: const Color(0xff1b447b),
                      onPressed: () async {

                        SharedPreferences prefs=await SharedPreferences.getInstance();
                        var userID=prefs.getString('userID');
                        var token=prefs.getString('token');

                        var url="http://10.0.2.2:4000/user/booking";

                        for(int i =0; i<cart.length;i++){
                          print(cart[i]['Amount']);
                          final http.Response response=await http.post(
                              url,headers:<String, String>{
                            'Content-type': 'application/json; charset=utf-8',HttpHeaders.authorizationHeader: 'Bearer $token'
                          },
                              body:jsonEncode(<String, String>{
                                'UserID': userID,'TicketID':cart[i]['TicketID'].toString(), 'CategoryName': cart[i]['CategoryName'],
                                'NumberOFTickets': cart[i]['TicketPurchased'].toString(),
                                'PaymentType':'Visa/Master', 'Amount':cart[i]['Amount'].toString(), 'Event_Name': cart[i]['Event_Name']
                              }));

                            if(response.statusCode==200){
                              if (formKey.currentState!.validate()) {

                                _showValidDialog(context,"Purchased","Tikcets Successfully purchased");
                              } else {
                                print('invalid!');
                              }

                            }
                          }



                      },
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future <AlertDialog> _showValidDialog(BuildContext context, String title, String content,) {
    showDialog<AlertDialog>(
      context: context,
      builder: (BuildContext context) {
        return  AlertDialog(
          backgroundColor: Color(0xff1b447b),
          title: Text(title),
          content: Text(content),
          actions: [
            FlatButton(
                child: Text(
                  "Ok",
                  style: TextStyle(fontSize: 18,color: Colors.cyan),
                ),
                onPressed: () async {
                  SharedPreferences prefs=await SharedPreferences.getInstance();
                  var userID=prefs.getString('userID');
                  var url="http://10.0.2.2:4000/user/cart/$userID";
                  var response=await http.delete(
                    url,headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                  },);
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return EventList();
                  }));
                }),
          ],
        );

      },
    );
    throw("error");

  }


  void onCreditCardModelChange(CreditCardModel creditCardModel) {
    setState(() {
      cardNumber = creditCardModel.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}