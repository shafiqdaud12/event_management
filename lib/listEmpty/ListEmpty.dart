import 'package:flutter/material.dart';

import '../constant.dart';

class ListEmpty extends StatefulWidget {
  const ListEmpty({Key? key}) : super(key: key);

  @override
  _ListEmptyState createState() => _ListEmptyState();
}

class _ListEmptyState extends State<ListEmpty> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Column(
        children: [
          Center(child: Text("No Tickets Available",style: TextStyle(fontSize:20,fontWeight: FontWeight.bold),)),
          SizedBox(height:20),
          Center(child: Text("We are all sold out",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500)))
        ],
      ),

    );
  }
}
