const Joi=require('joi');
const express= require('express');
const app= express();

app.use(express.json());


events=[
{id:1, name:'Independence Day'},
{id:2, name:'New Year'},
{id:3,name:'Concert Night with Atif '}
];

app.get('/',(req,res)=>{
    res.send('hello world, how are you');
});

app.get('/api/events',(req,res)=>{
    res.send(events );
});

app.get('/api/events/:id',(req,res)=>{
    const event= events.find(c => c.id==parseInt(req.params.id));
    if(!event) return res.status(404).send('event not found')
    res.send(event);
});

app.get('/api/users',(req,res)=>{
    res.send([1,2,3,4,5] );
});

app.post('/api/events',(req,res)=>{

    const { error }=validateEvent(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    const event={
        id: events.length+1,
        name:req.body.name
    };
    events.push(event);
    res.send(event);
});

app.put("/api/events/:id",(req,res)=>{
    //searching
    const event= events.find(c => c.id==parseInt(req.params.id));
    if(!event) return res.status(404).send('event not found')


    //validation
    const { error }=validateEvent(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    //update
    event.name=req.body.name;
    res.send(event);
});

app.delete('/api/events/:id',(req,res)=>{
    const event= events.find(c => c.id==parseInt(req.params.id));
    if(!event) return res.status(404).send('event not found')

    const index=events.indexOf(event);
    events.splice(index,1);

    res.send(event);


});

function validateEvent(event){
    const schema={
        name: Joi.string().min(3).required()
    };
    return  Joi.validate(event, schema);

}

const port= process.env.PORT || 3000;
app.listen(port, ()=> console.log(`Listening on port ${port}...`));