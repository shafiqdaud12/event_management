import 'package:eventmanagement/constant.dart';
import 'package:eventmanagement/login/loginScreen.dart';
import 'package:eventmanagement/login/roundedButton.dart';
import 'package:eventmanagement/register/registerScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Body extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;

    return Container(
      height: size.height,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: size.height*0.02,),
          Text(
            'Welcome to Event Management System',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SvgPicture.asset('svg/eventcheck.svg',
          height: size.height*0.3,),
          SizedBox(height: size.height*0.05,),

          RoundedButton(
            text: 'Login',
            press: (){
              Navigator.push(
                context,
              MaterialPageRoute(builder: (context){
                return loginScreen();
              },),
              );
            }
          ),

          RoundedButton(
            text: 'SignUp',
            color: kPrimaryLightColor,
            press: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return registerScreen();
              }));
            },
            textColor: Colors.black,
          ),

        ],
      ),

    );
  }
}





