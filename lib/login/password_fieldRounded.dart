import 'package:eventmanagement/constant.dart';
import 'textFieldContainerInput.dart';
import 'package:flutter/material.dart';


class roundedPasswordField extends StatefulWidget {
  final ValueChanged<String> onChanged;
  final String hintText;

  const roundedPasswordField({
    Key? key, required this.onChanged, required this.hintText,

  }) : super(key: key);

  @override
  _roundedPasswordFieldState createState() => _roundedPasswordFieldState();
}

class _roundedPasswordFieldState extends State<roundedPasswordField> {
  bool obsecuretext=true;

  @override
  Widget build(BuildContext context) {

    return textFieldContainer(child: TextFormField(
      obscureText: obsecuretext,
      onChanged: widget.onChanged,
      decoration: InputDecoration(
        hintText: widget.hintText,
        icon: Icon(Icons.lock,color: kPrimaryColor),
        suffixIcon: IconButton(icon: Icon(obsecuretext?Icons.visibility:Icons.visibility_off,color: kPrimaryColor, ),
          onPressed: () {
          setState(() {
            obsecuretext=!obsecuretext;
          });
          },),
        border: InputBorder.none,

      ),
    ));
  }
}
