
import 'package:flutter/material.dart';

class textFieldContainer extends StatefulWidget {
  final Widget child;

  const textFieldContainer({
    Key? key, required this.child,
  }) : super(key: key);

  @override
  _textFieldContainerState createState() => _textFieldContainerState();
}

class _textFieldContainerState extends State<textFieldContainer> {
  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(vertical: 5,horizontal: 20),
      width: size.width*0.8,
      decoration: BoxDecoration(
        color: Colors.black26,
        borderRadius: BorderRadius.circular(29),
      ),
      child: widget.child,

    );
  }
}
