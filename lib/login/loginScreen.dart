
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:eventmanagement/constant.dart';
import 'package:eventmanagement/eventList/eventlist.dart';
import 'package:eventmanagement/login/password_fieldRounded.dart';
import 'package:eventmanagement/login/roundedButton.dart';
import 'package:eventmanagement/login/rounded_inputfield.dart';
import 'package:eventmanagement/register/registerScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class loginScreen extends StatefulWidget {
  const loginScreen({Key? key}) : super(key: key);

  @override
  _loginScreenState createState() => _loginScreenState();
}

class _loginScreenState extends State<loginScreen> {

  String password='';
  String EmailAddress='';

  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;



    return Scaffold(
      body: Container(

    width: double.infinity,
        height:size.height ,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('LOGIN',style: TextStyle(fontWeight: FontWeight.bold),),
            Image.asset('svg/login2.png',height: size.height*0.3,),
            RoundedFieldContainer(hintText: "Your Email ID", onChanged: (value){
              EmailAddress=value;
            }),

            roundedPasswordField(onChanged: (value){
              password=value;
            },hintText: 'Enter Password',),
            RoundedButton(text: 'Login' ,press:() async {
              var url='http://10.0.2.2:4000/user/login';

              final http.Response response=await http.post(url,headers: <String,String>{
                'Content-Type':"application/json; charset=UTF-8",},
                body: jsonEncode(<String,String>{
                  'EmailAddress':EmailAddress,
                  'password':password
                }),

              );

              if(response.statusCode == 403){
                AlertDialog alert= AlertDialog(title: Text("EmailId/Password is wrong"),
                content: Text("Enter correct id or password"),
                actions: [],);

                showDialog(context: context, builder: (BuildContext context){
                  return alert;
                });
              }
              else if(response.statusCode==200) {
                if(EmailAddress!='' && password !='' && password.length >=8 ){
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  var parse = jsonDecode(response.body);//parsing data coming from db
                  await prefs.setString('token', parse['token']);//saving token value as a string
                  await prefs.setString('userName', parse['userName']);
                  await prefs.setString('userID', parse['user']);
                  String token = prefs.getString("token");//Reading the value and storing it in token.
                  String userID=prefs.getString("userID");
                  // print(userID);
              Navigator.push(context, MaterialPageRoute(builder: (context){
                    return EventList();
                  }));
                }else if(password.length <8){
                  AlertDialog alert= AlertDialog(title: Text("Password too short"),
                    content: Text("Password should be greater than 8 characters"),
                    actions: [],);

                  showDialog(context: context, builder: (BuildContext context){
                    return alert;
                  });
                }
              }else{
                print("Error");
              }

            }),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Don't Have an account ? ",
                  style: TextStyle(color:kPrimaryColor),
                ),
                GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context){
                        return registerScreen();
                      }));
                    },
                    child: Text('SIGN UP',
                      style: TextStyle(color: kPrimaryColor,
                          fontWeight: FontWeight.bold),),
                ),

              ],

            ),
          ],

        ),
      ),
    );
  }
}

