import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:eventmanagement/Booking%20detail/BookingDetail.dart';
import 'package:eventmanagement/CartScreen/CartScreen.dart';
import 'package:eventmanagement/TicketScreen/ticketScreen.dart';
import 'package:eventmanagement/constant.dart';
import 'package:eventmanagement/welcome.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class EventList extends StatefulWidget {
  const EventList({Key? key}) : super(key: key);

  @override
  _EventListState createState() => _EventListState();
}

class _EventListState extends State<EventList> {
  List eventList=[];
  late String Event_ID;
  String name='';
  List dateTime=[];

  //getting list of events form Database
  getAllEvents() async{
    var url="http://10.0.2.2:4000/user/events";
     SharedPreferences prefs=await SharedPreferences.getInstance();
     var token= prefs.getString('token');//taking token from login screen

    final http.Response response=await http.get(url,headers: <String,String>{
    'Content-Type':"application/json; charset=UTF-8",HttpHeaders.authorizationHeader: 'Bearer $token'});
    if(response.statusCode==200   ){
      setState(() {
        eventList=jsonDecode(response.body);
        name=prefs.getString('userName');
      });
      print(name);
      for(int i=0;i<eventList.length;i++){
        setState(() {
          dateTime.add(eventList[i]['DateTime']);
        });
      }

      return eventList;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllEvents();
  }

  @override
  Widget build(BuildContext context) {
    Size size= MediaQuery.of(context).size;


    return Scaffold(

      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        title: Text("Welcome to Event Screen"),
        actions: [
          IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context){
              return BookingScreen();
            }));
          }, icon: Icon(Icons.shopping_cart)),
        ],
      ),

      body: Container(
        //color:Colors.grey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.0)
                    ),
                    height: size.width,
                  ),

                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: 30,horizontal: 20
                    ),
                    child: Row(
                      children: <Widget>[
                        Text(
                            'Hello $name ! \nLets explore events' ,
                          style: GoogleFonts.spartan(
                            fontSize: 20,
                         //   color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:EdgeInsets.symmetric(vertical:120),
                        child: Row(
                          children:[
                            Expanded(child:Divider(
                              color: Colors.black,
                            )),
                            Text('Events',
                            style: GoogleFonts.spartan(
                              fontSize: 18,
                              color: Colors.black
                            ),
                          ),
                            Expanded(child:Divider(
                              color:Colors.black,
                            )),
                          ],
                        ),
                  ),

                  Padding (
                    padding: EdgeInsets.symmetric(vertical:160,horizontal:20),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Container(
                        height:size.height*.7,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemCount: eventList.length ,
                          itemBuilder: (context,index){
                            return Card(
                              shadowColor: Colors.purple,
                             // color: Colors.grey[400],
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                              elevation: 5,
                              child: EventTile(
                                eventName: eventList[index]['Event_Name'],
                                location: eventList[index]['Location'],
                                Date:    DateTime.parse(eventList[index]['DateTime']).toString().split(' ')[0],
                                Time:    DateTime.parse(eventList[index]['DateTime']).toString().split(' ')[1].split('.')[0],
                                eventID: eventList[index]['Event_ID'].toString(),
                              ),
                            );

                          },

                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(color: kPrimaryColor),
                child: Text('Menu',style:TextStyle(fontSize: 22)),
            ),
            ListTile(
              title: Text('Bookings'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return BookingDetail();
                }));
              },
            ),
            ListTile(
              title: Text('Cart'),
              onTap: () async {
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return BookingScreen();
                }));
              },
            ),
            ListTile(
              title: Text('Logout'),
              onTap: () async {
                var count=0;
                SharedPreferences prefs= await SharedPreferences.getInstance();
                String token=prefs.get('token');
                var UserID=prefs.getString("userID");
                print(UserID);
                //debugger();

                var url="http://10.0.2.2:4000/user/cart/$UserID";
                await http.delete(url,headers: <String, String>{
                  'Content-type': 'application/json; charset=utf-8',HttpHeaders.authorizationHeader: 'Bearer $token'
                });

                prefs.setString('token',null);



                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return WelcomeScreen();
                }));
              },
            ),

          ],
        ),
      ),
    );

  }
}

class EventTile extends StatelessWidget {
  String  eventName='';
  String  Date='';
  String Time='';
  String location='';
  String eventID='';

  EventTile({required this.eventName,required this.Date,required this.Time,required this.location,required this.eventID});

  @override
  Widget build(BuildContext context) {

    return Container(
      margin:EdgeInsets.only(bottom: 8),
      padding: EdgeInsets.symmetric(horizontal:20,vertical:10),
      child:Row(
        children: [
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height:10),
                  Row(
                    children: <Widget>[
                      Text('Name'),
                      SizedBox(width:32),
                      Text(eventName,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),)
                    ],
                  ),
                  SizedBox(height:10),
                  Row(
                    children: <Widget>[
                      Text('Date'),
                      SizedBox(width:41),
                      Text(Date,style: TextStyle(fontSize: 18)),
                    ],
                  ),
                  SizedBox(height:10),
                  Row(
                    children: <Widget>[
                      Text('Time'),
                      SizedBox(width:40),
                      Text(Time,style: TextStyle(fontSize: 18))
                    ],
                  ),
                  SizedBox(height:10),
                  Row(
                    children: <Widget>[
                      Text('Location' ),
                      SizedBox(width:20),
                      Text(location,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold))
                    ],
                  ),
                  SizedBox(height:10),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context){
                        return ticketScreen(Event_ID: eventID);
                      }));
                    },
                    child: Text("View Ticket Details"),
                    style: ElevatedButton.styleFrom(primary: kPrimaryColor),
                  ),


                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}



