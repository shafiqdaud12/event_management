import 'package:flutter/material.dart';
import '../constant.dart';

class loadingScreen extends StatelessWidget {
  const loadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CircularProgressIndicator(
          color: kPrimaryColor,

        ),
      ),
    );
  }
}
