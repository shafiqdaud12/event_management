
import 'package:eventmanagement/constant.dart';
import 'package:eventmanagement/login/textFieldContainerInput.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class RoundedFieldContainer extends StatefulWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const RoundedFieldContainer({
    Key? key, required this.hintText,
    this.icon= Icons.person,
    required this.onChanged,
  }) : super(key: key);

  @override
  _RoundedFieldContainerState createState() => _RoundedFieldContainerState();
}

class _RoundedFieldContainerState extends State<RoundedFieldContainer> {
  @override
  Widget build(BuildContext context) {
    return textFieldContainer(child: TextField(
      onChanged: widget.onChanged,
      decoration: InputDecoration(
          icon: Icon(widget.icon,color: kPrimaryColor,),
          hintText: widget.hintText,
          border: InputBorder.none)
      ,),);
  }
}
